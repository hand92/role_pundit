class RolesController < ApplicationController
  before_action :set_role, only: %i[show edit update destroy]

  def index
  	@roles = Role.all
  end

  def new
  	@role = Role.new
  end

  def show
  	
  end

  def create
    @role = Role.new(role_params)

    if @role.save
      redirect_to roles_url, notice: "Role was successfully created."
    else
      render :new
    end
  end

  def edit
  	
  end

  # PATCH/PUT /roles/1
  def update
    if @role.update(role_params)
      redirect_to roles_url, notice: "Role was successfully updated."
    else
      render :edit
    end
  end

  def destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_role
    @role = Role.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def role_params
    params.require(:role).permit(:name, permissions_attributes: {})
  end

end
