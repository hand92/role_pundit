class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :role_assignments, dependent: :destroy
  has_many :roles, through: :role_assignments

  def permissions
    roles.map(&:permissions).flatten
  end

  def can?(&block)
    roles.map(&:permissions).any?(&block)
  end

  # def can?(&block)
  #   roles.map(&:permissions).any?(&block)
  # end

  # def computed_permissions
  #   roles.map(&:computed_permissions).reduce(RoleCore::ComputedPermissions.new, &:concat)
  # end
end
