module RoleCorePundit
  class BasePolicy
    attr_reader :user, :record

    def initialize(user,record)
      @user = user
      @record = record
    end

    def method_missing(method, *args, &block)
      if method.to_s =~ /\?$/
        allowed?(method)
      else
        super
      end
    end

    def allowed?(action)
      return false if record.nil?
      # binding.pry
      prmissions_group_key = record.model_name.param_key.to_sym
      # user.permissions.public_send(prmissions_group_key).public_send(action)
      user.can? { |permissions| permissions.public_send(prmissions_group_key).public_send(action)}
    end

    class Scope
      attr_reader :user, :scope

      def initialize(user, scope)
        @user = user
        @scope = scope
      end

      def resolve
        scope
      end
    end
  end
end