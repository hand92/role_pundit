Rails.application.routes.draw do
	
  resources :articles do
  	resources :comments
  end
  resources :roles

  resources :accounts
  
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  root 'home#index'

  # mount RuCaptcha::Engine => "/rucaptcha"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
