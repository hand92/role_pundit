class CreateRoleAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :role_assignments do |t|
      t.references :user, foreign_key: true
      t.references :role, foreign_key: true

      t.timestamps
    end

    add_foreign_key :role_assignments, :users
    add_foreign_key :role_assignments, :roles
  end
end
